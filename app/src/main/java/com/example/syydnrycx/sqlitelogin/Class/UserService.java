package com.example.syydnrycx.sqlitelogin.Class;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.syydnrycx.sqlitelogin.SQLite.DatabaseHelper;

import java.util.ArrayList;


public class UserService {
    //private ArrayList<Integer> useridList = new ArrayList<>();
    private ArrayList<String> usernameList = new ArrayList<>();
    //private ArrayList<String> userpasswordlList = new ArrayList<>();
    private DatabaseHelper dbHelper;
    private String s = null;

    public UserService(Context context){
        dbHelper=new DatabaseHelper(context);
    }

    
    public boolean login(String username,String password){
        SQLiteDatabase sdb=dbHelper.getReadableDatabase();
        String sql="select * from user where username=? and password=?";
        Cursor cursor=sdb.rawQuery(sql, new String[]{username,password});
        if(cursor.moveToFirst()==true){
            cursor.close();
            return true;
        }
        return false;
    }

    
    public boolean register(User user){
        
        SQLiteDatabase sdb=dbHelper.getReadableDatabase();
        String sql="insert into user(username,password) values(?,?)";
        Object obj[]={user.getUsername(),user.getPassword()};
        sdb.execSQL(sql, obj);
        return true;
    }

    public ArrayList<String> getAll() {
        SQLiteDatabase sdb=dbHelper.getReadableDatabase();
        
        Cursor cursor = sdb.query ("user",null,null,null,null,null,null);
        
        if(cursor.moveToFirst()) {
            
            do{
                
                usernameList.add(cursor.getString(1));
            }while(cursor.moveToNext());
            
            cursor.close();
        }

        return usernameList;

    }


}
