package com.example.syydnrycx.sqlitelogin.Activity;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListPopupWindow;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import com.example.syydnrycx.sqlitelogin.Class.UserService;
import com.example.syydnrycx.sqlitelogin.R;
import com.mob.MobSDK;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import cn.smssdk.EventHandler;
import cn.smssdk.SMSSDK;


public class LoginActivity extends Activity implements View.OnClickListener{

    private ArrayList<String> usernamelList;

    private Button bt_login,bt_register;
    private ImageButton image_btn;
    private EditText edit_username;
    private EditText edit_password;
    private EditText edit_phone;


    private UserService uService = null;
    private ListPopupWindow listPopupWindow;
    EventHandler eventHandler;
    private boolean coreflag=true;

    private Context context;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.logic_layout);

        initViews();


    }

    private void initViews() {

        bt_login=(Button) findViewById(R.id.bt_login);
        bt_register=(Button) findViewById(R.id.bt_register);
        image_btn=(ImageButton)findViewById(R.id.user_btn_img);

        edit_username=(EditText) findViewById(R.id.user_name);
        edit_password=(EditText) findViewById(R.id.user_pass);
        edit_phone=(EditText)findViewById(R.id.ed_phone);


        bt_login.setOnClickListener(this);
        bt_register.setOnClickListener(this);
        image_btn.setOnClickListener(this);

        uService = new UserService(LoginActivity.this);

        usernamelList = uService.getAll();

    }

    protected void onDestroy() {
        super.onDestroy();
        SMSSDK.unregisterEventHandler(eventHandler);

    }

    protected void onResume() {
        super.onResume();
        usernamelList.clear();
        usernamelList = uService.getAll();
    }


    private void showListPopulWindow() {

        listPopupWindow = new ListPopupWindow(this);
        listPopupWindow.setAdapter(new ArrayAdapter<String>(this,R.layout.list_item, usernamelList));
        listPopupWindow.setAnchorView(edit_username);
        listPopupWindow.setModal(true);

        listPopupWindow.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                edit_username.setText(usernamelList.get(i));
                listPopupWindow.dismiss();
            }

        });

        listPopupWindow.show();
    }







    @Override
    public void onClick(View v) {


        switch (v.getId()){
            case R.id.bt_login:
                String name=edit_username.getText().toString();
                String pass=edit_password.getText().toString();
                boolean flag=uService.login(name, pass);
                if(flag){
                    Log.i("TAG","Sesion iniciada");
                    Intent intent=new Intent(LoginActivity.this,MainActivity.class);
                    startActivity(intent);
                    Toast.makeText(LoginActivity.this, "Inicio correcto", Toast.LENGTH_LONG).show();
                }else{
                    Log.i("TAG","Error al iniciar");
                    Toast.makeText(LoginActivity.this, "Error al iniciar", Toast.LENGTH_LONG).show();
                }
                break;
            case R.id.bt_register:
                Intent intent=new Intent(LoginActivity.this,RegisterActivity.class);
                startActivity(intent);
                break;

            case R.id.user_btn_img:
                showListPopulWindow();
                break;
        }
    }



}